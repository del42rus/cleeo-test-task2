<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\UselessEntity;

class UselessEntityController extends FOSRestController
{
    /**
     * @Rest\Get("/useless-entities")
     */
    public function getAction()
    {
        $result = $this->getDoctrine()->getRepository('AppBundle:UselessEntity')->findAll();

        return $result;
    }

    /**
     * @Rest\Get("/useless-entities/{id}")
     */
    public function idAction($id)
    {
        $result = $this->getDoctrine()->getRepository('AppBundle:UselessEntity')->find($id);

        if ($result === null) {
            return new View('Entity not found', Response::HTTP_NOT_FOUND);
        }

        return $result;
    }

    /**
     * @Rest\Post("/useless-entities")
     */
    public function postAction(Request $request)
    {
        $entity = new UselessEntity();

        $data = json_decode($request->getContent(), true);

        if (isset($data['meh'])) {
            $entity->setMeh($data['meh']);
        }

        if (isset($data['whatever'])) {
            $entity->setWhatever($data['whatever']);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        return $entity;
    }

    /**
     * @Rest\Put("/useless-entities/{id}")
     */
    public function updateAction($id, Request $request)
    {
        $entity = $this->getDoctrine()->getRepository('AppBundle:UselessEntity')->find($id);

        if (!$entity) {
            return new View('Entity not found', Response::HTTP_NOT_FOUND);
        }

        $data = json_decode($request->getContent(), true);

        if (isset($data['meh'])) {
            $entity->setMeh($data['meh']);
        }

        if (isset($data['whatever'])) {
            $entity->setWhatever($data['whatever']);
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $entity;
    }

    /**
     * @Rest\Delete("/useless-entities/{id}")
     */
    public function deleteAction($id)
    {
        $entity = $this->getDoctrine()->getRepository('AppBundle:UselessEntity')->find($id);

        if (!$entity) {
            return new View('Entity not found', Response::HTTP_NOT_FOUND);
        }

        $em = $this->getDoctrine()->getManager();

        $em->remove($entity);
        $em->flush();

        return new View('Entity deleted successfully', Response::HTTP_OK);
    }
}