DROP TABLE IF EXISTS `useless_entity`;

CREATE TABLE `useless_entity` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`meh` VARCHAR(255),
	`whatever` VARCHAR(255),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET='utf8';

INSERT INTO `useless_entity` VALUES (1, 'Foo', 'Bar'), (2, 'Foo', 'Baz');