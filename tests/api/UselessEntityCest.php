<?php


class UselessEntityCest
{
    public function _before(ApiTester $I)
    {

    }

    public function _after(ApiTester $I)
    {
    }

    public function getAll(ApiTester $I)
    {
        $I->sendGET('useless-entities');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }

    public function view(ApiTester $I)
    {
        $I->sendGET('useless-entities/1');
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson([
            'meh' => 'Foo',
            'whatever' => 'Bar'
        ]);
    }

    public function viewNotFound(ApiTester $I)
    {
        $I->sendGET('useless-entities/3');
        $I->seeResponseCodeIs(404);
    }

    public function create(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('useless-entities', [
            'meh' => 'Test Meh',
            'whatever' => 'Test Whatever'
        ]);

        $I->seeResponseCodeIs(200);

        $I->seeResponseContainsJson([
            'meh' => 'Test Meh',
            'whatever' => 'Test Whatever'
        ]);

        $I->seeInDatabase('useless_entity', [
            'meh' => 'Test Meh',
            'whatever' => 'Test Whatever'
        ]);
    }

    public function update(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPUT('useless-entities/1', [
            'meh' => 'Bar',
            'whatever' => 'Foo'
        ]);

        $I->seeResponseCodeIs(200);

        $I->seeResponseContainsJson([
            'meh' => 'Bar',
            'whatever' => 'Foo'
        ]);

        $I->seeInDatabase('useless_entity', [
            'meh' => 'Bar',
            'whatever' => 'Foo'
        ]);
    }

    public function updateNotFound(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPUT('useless-entities/4', [
            'meh' => 'Bar',
            'whatever' => 'Foo'
        ]);

        $I->seeResponseCodeIs(404);
    }

    public function delete(ApiTester $I)
    {
        $I->sendDELETE('useless-entities/3');
        $I->seeResponseCodeIs(200);
    }

    public function deleteNotFound(ApiTester $I)
    {
        $I->sendDELETE('useless-entities/4');
        $I->seeResponseCodeIs(404);
    }
}
