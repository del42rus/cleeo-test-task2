## Installation
Go to the root directory. Run the following command
```sh
$ composer install
```

## Run web server
```sh
$ php bin/console server:run
```

## Prepare database
```sh
$ php bin/console doctrine:schema:update --force
```

## REST API

**Get list of useless entities**
```sh
$ curl --request GET http://127.0.0.1:8000/useless-entities
```

**Get useless entity**
```sh
$ curl --request GET http://127.0.0.1:8000/useless-entities/1
```

**Create useless entity**
```sh
$ curl --request POST --data '{"meh": "Foo", "whatever": "Bar"}' http://127.0.0.1:8000/useless-entities
```

**Update useless entity**
```sh
$ curl --request PUT --data '{"meh": "Bar", "whatever": "Foo"}' http://127.0.0.1:8000/useless-entities/1
```

**Delete useless entity**
```sh
$ curl --request DELETE http://127.0.0.1:8000/useless-entities/1
```

## Run Tests
1. Change database connection settings in the file `tests/api.suite.yml`

2. Run the  command
```sh
$ php vendor/bin/codecept run api
```